Transfer from Schematic to PCB.
-------------------------------

Report Written:     Friday, February 07, 2020
Design Path:        C:\Users\Chris\Desktop\CAT5 Tester v0.1\schCAT5TesterV0.1.pcb
Design Title:       
Created:            2020/02/07 12:31:24
Last Saved:         2020/02/07 12:31:42
Editing Time:       0 min
Units:              mm (precision 2)

Net class "Power" was not defined, but has been created. Please check the values in the PCB design.
Net class "Ground" was not defined, but has been created. Please check the values in the PCB design.
Net class "Signal" was not defined, but has been created. Please check the values in the PCB design.

End Of Report.
